/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    colors: {
      'background': '#464545FF',
      'background-soft': '#222222FF',
      'background-mute': '#282828FF',
      'border': '#5454547A',
      'vert': '#EBEBEBA3',
      'vert-two': '#509b46',
      'gray-dark': '#273444',
      'gray': '#8492a6',
      'gray-light': '#d3dce6',
    },
    extend: {},
  },
  plugins: [],
}

