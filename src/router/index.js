import Login from '../views/Login.vue'
import CreationBludtz from "@/views/CreationBludtz.vue";
import ResetPassword from "@/views/ResetPassword.vue";
import Anime from "@/views/Anime.vue";

import {createRouter, createWebHistory} from "vue-router";
import {getUserMe} from "@/services";
import {reactive} from "vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/create',
      name: 'Create',
      component: CreationBludtz
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword
    },
    {
      path: '/anime',
      name: 'Anime',
      component: Anime
    }
  ]
})

const user = reactive({
  localStorage: {},
  authenticated: false
});

router.beforeEach(async (to, from) => {
  user.localStorage = await JSON.parse(localStorage.getItem("user")) || false
  await getUserMe(user.localStorage.jwt)
      .then(() => {
        user.authenticated = true
      })
      .catch(() => {
        user.authenticated = false
      })

  if (!user.authenticated && to.name !== 'login') return '/'
})

export default router
