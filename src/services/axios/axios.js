import Axios from "axios";

const urlApi = import.meta.env.VITE_STRAPI_URL;

const instance = Axios.create({
		baseURL: urlApi,
		responseType: 'json'
	}
)

const axios = instance;

export { axios }
