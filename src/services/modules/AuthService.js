import { axios } from "@/services/axios/axios";

export const postRegister = (username, email, password) => {
	return  axios
		.post(import.meta.env.VITE_STRAPI_URL + '/api/auth/local/register', {
			'username': username,
			'email': email,
			'password': password
		})
		.then(({data}) => {
			localStorage.setItem("user", JSON.stringify(data));
			return Promise.resolve(data)
		})
		.catch((error) => {
			return Promise.reject(error)
		})
}

export const postLogin = (email, password) => {
	return axios
		.post(import.meta.env.VITE_STRAPI_URL + '/api/auth/local', {
			identifier: email,
			password: password,
		})
		.then(({data}) => {
			localStorage.setItem("user", JSON.stringify(data));
			return Promise.resolve(data)
		})
		.catch((error) => {
			return Promise.reject(error)
		});
}

export const getUserMe = (token) => {
	return axios.get('/api/users/me', { headers: {'Authorization': `Bearer ${token}`}})
		.then(({data}) => {
			return Promise.resolve(data)
		})
		.catch( (error) => {
			return Promise.reject(error)
		});
};

export const postForgotPassword = (email) => {
	return axios
		.post(import.meta.env.VITE_STRAPI_URL + '/api/auth/forgot-password', {
			email: email, // user's email
		})
		.then(({data}) => {
			return Promise.resolve(data)
		})
		.catch(error => {
			return Promise.reject(error)
		});
}

export const postResetPassword = (code, password, confirmPassword) => {
	return axios
		.post(import.meta.env.VITE_STRAPI_URL + '/api/auth/reset-password', {
				code: code,
				password: password,
				passwordConfirmation: confirmPassword,
			})
				.then(({data}) => {
					localStorage.setItem("user", JSON.stringify(data));
					return Promise.resolve('Password changed')
				})
				.catch(error => {
					return Promise.reject(error)
				});
}
